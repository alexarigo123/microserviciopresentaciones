from myapp import db, ma
from sqlalchemy.sql import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

# Modelo de la tabla Presentacion.
class Presentacion(db.Model):
    __tablename__ = 'presentacion'

    id = db.Column(db.Integer, primary_key=True)
    id_usuario = db.Column(db.Integer, nullable=True)
    nombre = db.Column(db.String(100), unique=True, nullable=False)
    diapositivas = relationship("Diapositiva", back_populates="presentacion")
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, nombre, id_usuario=None):
        self.nombre = nombre
        if id_usuario is not None:
            self.id_usuario = id_usuario


# Esquema para exportar como JSON entidades de la tabla Presentacion.
class PresentacionSchema(ma.Schema):
    class Meta:
        fields = ('id', 'id_usuario', 'nombre')


presentacion_schema = PresentacionSchema()
presentaciones_schema = PresentacionSchema(many=True)


# Modelo de la tabla Diapositiva.
class Diapositiva(db.Model):
    __tablename__ = 'diapositiva'

    id = db.Column(db.Integer, primary_key=True)
    id_presentacion = db.Column(db.Integer, ForeignKey('presentacion.id'), nullable=False)
    presentacion = relationship("Presentacion", back_populates="diapositivas")
    titulo = db.Column(db.String(100), nullable=True)
    contenido = db.Column(db.String(4000), nullable=False)
    tipo = db.Column(db.String(100), nullable=False)
    orden = db.Column(db.Integer, nullable=False)
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, id_presentacion, titulo, contenido, tipo, orden):
        self.id_presentacion = id_presentacion
        self.titulo = titulo
        self.contenido = contenido
        self.tipo = tipo
        self.orden = orden


# Esquema para exportar como JSON entidades de la tabla Diapositiva.
class DiapositivaSchema(ma.Schema):
    class Meta:
        fields = ('id', 'id_presentacion', 'titulo', 'contenido', 'tipo', 'orden')


diapositiva_schema = DiapositivaSchema()
diapositivas_schema = DiapositivaSchema(many=True)
