from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import app, db, ma
from models import *

#endpoint para crear una nueva presentacion.
@app.route("/presentacion", methods=["POST"])
@cross_origin()
def add_presentacion():
    id_usuario = request.json["id_usuario"]
    nombre = request.json["nombre"]
    diapositivas = request.json["diapositivas"]

    new_presentacion = Presentacion(nombre, id_usuario)

    db.session.add(new_presentacion)
    db.session.commit()

    if diapositivas is not None:
        print(diapositivas)
        for diapositiva in diapositivas:
            if diapositiva["id"] == 0:
                titulo = diapositiva["titulo"]
                contenido = diapositiva["contenido"]
                tipo = diapositiva["tipo"]
                orden = diapositiva["orden"]
                if titulo is not None and contenido is not None and tipo is not None and orden is not None:
                    new_diapositiva = Diapositiva(new_presentacion.id, titulo, contenido, tipo, orden)
                    db.session.add(new_diapositiva)
            else:
                edit_diapositiva = Diapositiva.query.get(diapositiva["id"])
                if edit_diapositiva is not None:
                    if "titulo" in diapositiva:
                        edit_diapositiva.titulo = diapositiva["titulo"]
                    if "contenido" in diapositiva:
                        edit_diapositiva.contenido = diapositiva["contenido"]
                    if "tipo" in diapositiva:
                        edit_diapositiva.tipo = diapositiva["tipo"]
                    if "orden" in diapositiva:
                        edit_diapositiva.orden = diapositiva["orden"]
        db.session.commit()

    return presentacion_schema.jsonify(new_presentacion)


#endpoint para devolver todas las presentaciones.
@app.route("/presentacion", methods=["GET"])
@cross_origin()
def get_presentaciones():
    all_presentaciones = Presentacion.query.all()
    if all_presentaciones is None:
        return jsonify({})

    result = presentaciones_schema.dump(all_presentaciones)

    return jsonify(result.data)


#endpoint para devolver una sola presentacion por id.
@app.route("/presentacion/<id>", methods=["GET"])
@cross_origin()
def get_presentacion(id):
    presentacion = Presentacion.query.get(id)
    if presentacion is None:
        return jsonify({})

    return presentacion_schema.jsonify(presentacion)


#endpoint para devolver una sola presentacion por id con la información de sus diapositivas.
@app.route("/presentacion/<id>/all", methods=["GET"])
@cross_origin()
def get_presentacion_all(id):
    presentacion = Presentacion.query.get(id)
    if presentacion is None:
        return jsonify({})

    result_dict = {
        'presentacion': presentacion.nombre,
        'id_usuario': presentacion.id_usuario,
        'diapositivas': []
    }

    if presentacion.diapositivas is not None:
        for diapositiva in presentacion.diapositivas:
            nueva_diapositiva = {'id': diapositiva.id, 'titulo': diapositiva.titulo, 'contenido': diapositiva.contenido, 'tipo': diapositiva.tipo, 'orden': diapositiva.orden}

            result_dict['diapositivas'].append(nueva_diapositiva)

    return jsonify(result_dict)


#endpoint para actualizar una presentacion.
@app.route("/presentacion/<id>", methods=["PUT"])
@cross_origin()
def update_presentacion(id):
    presentacion = Presentacion.query.get(id)
    if presentacion is None:
        return jsonify({})

    if "id_usuario" in request.json:
        presentacion.id_usuario = request.json["id_usuario"]
    if "nombre" in request.json:
        presentacion.nombre = request.json["nombre"]

    if "diapositivas" in request.json:
        diapositivas = request.json["diapositivas"]
        for diapositiva in diapositivas:
            if diapositiva["id"] == 0:
                titulo = diapositiva["titulo"]
                contenido = diapositiva["contenido"]
                tipo = diapositiva["tipo"]
                orden = diapositiva["orden"]
                if titulo is not None and contenido is not None and tipo is not None and orden is not None:
                    new_diapositiva = Diapositiva(presentacion.id, titulo, contenido, tipo, orden)
                    db.session.add(new_diapositiva)
            else:
                edit_diapositiva = Diapositiva.query.get(diapositiva["id"])
                if edit_diapositiva is not None:
                    if "titulo" in diapositiva:
                        edit_diapositiva.titulo = diapositiva["titulo"]
                    if "contenido" in diapositiva:
                        edit_diapositiva.contenido = diapositiva["contenido"]
                    if "tipo" in diapositiva:
                        edit_diapositiva.tipo = diapositiva["tipo"]
                    if "orden" in diapositiva:
                        edit_diapositiva.orden = diapositiva["orden"]
    db.session.commit()

    return presentacion_schema.jsonify(presentacion)


#endpoint para borrar una presentacion.
@app.route("/presentacion/<id>", methods=["DELETE"])
@cross_origin()
def delete_presentacion(id):
    presentacion = Presentacion.query.get(id)
    if presentacion is None:
        return jsonify({})

    if presentacion.diapositivas is not None:
        for diapositiva in presentacion.diapositivas:
            db.session.delete(diapositiva)

    db.session.delete(presentacion)
    db.session.commit()

    return presentacion_schema.jsonify(presentacion)
