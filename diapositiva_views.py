from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import app, db
from models import *

#endpoint para crear una nueva diapositiva.
@app.route("/diapositiva", methods=["POST"])
@cross_origin()
def add_diapositiva():
    id_presentacion = request.json["id_presentacion"]
    titulo = request.json["titulo"]
    contenido = request.json["contenido"]
    tipo = request.json["tipo"]
    orden = request.json["orden"]

    new_diapositiva = Diapositiva(id_presentacion, titulo, contenido, tipo, orden)

    db.session.add(new_diapositiva)
    db.session.commit()

    return diapositiva_schema.jsonify(new_diapositiva)


#endpoint para devolver todas los diapositivas.
@app.route("/diapositiva", methods=["GET"])
@cross_origin()
def get_diapositivas():
    all_diapositivas = Diapositiva.query.all()
    if all_diapositivas is None:
        return jsonify({})

    result = diapositivas_schema.dump(all_diapositivas)

    return jsonify(result.data)


#endpoint para devolver una sola diapositiva por id.
@app.route("/diapositiva/<id>", methods=["GET"])
@cross_origin()
def get_diapositiva(id):
    diapositiva = Diapositiva.query.get(id)
    if diapositiva is None:
        return jsonify({})

    return diapositiva_schema.jsonify(diapositiva)


#endpoint para actulizar una diapositiva.
@app.route("/diapositiva/<id>", methods=["PUT"])
@cross_origin()
def update_diapositiva(id):
    diapositiva = Diapositiva.query.get(id)
    if diapositiva is None:
        return jsonify({})

    if "id_presentacion" in request.json:
        diapositiva.id_presentacion = request.json["id_presentacion"]
    if "titulo" in request.json:
        diapositiva.titulo = request.json["titulo"]
    if "contenido" in request.json:
        diapositiva.contenido = request.json["contenido"]
    if "tipo" in request.json:
        diapositiva.tipo = request.json["tipo"]
    if "orden" in request.json:
        diapositiva.orden = request.json["orden"]

    db.session.commit()

    return diapositiva_schema.jsonify(diapositiva)


#endpoint para borrar una diapositiva.
@app.route("/diapositiva/<id>", methods=["DELETE"])
@cross_origin()
def delete_diapositiva(id):
    diapositiva = Diapositiva.query.get(id)
    if diapositiva is None:
        return jsonify({})

    db.session.delete(diapositiva)
    db.session.commit()

    return diapositiva_schema.jsonify(diapositiva)
