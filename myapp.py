from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# Se importan las configuraciones.
app.config.from_pyfile("config.py")

db = SQLAlchemy(app)
ma = Marshmallow(app)

# Se importan las vistas.
from presentacion_views import *
from diapositiva_views import *

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5001)
